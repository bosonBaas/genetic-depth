import numpy as np

from classes.neuralNet import NeuralNet
from classes.evolver import Evolver
import classes.dataSets as ds

evolve = Evolver()

pop = evolve.createPop(input_width=200, siam_width=10, siam_depth=2, proc_width=10, proc_depth=1)
testXDat, testYDat = ds.genStereoDat(500, 100)
inXDat, inYDat = ds.genStereoDat(300, 100)
for t in xrange(500):
   pop, curPrec = evolve.iterateGeneration(pop, inXDat, inYDat)
   curOutPrec = evolve.getFitness(pop[0], testXDat, testYDat)
   print str(t) + ": "
   print "in:\t", curPrec
   print "out:\t", curOutPrec

myNN = pop[0]

xDat = np.linspace(-3, 3, 100)
yDat = np.linspace(0.25, 3, 4)

for y in yDat:
   actData = []
   apprData = []
   for x in xDat:
      curX, curY = ds.getStereo(np.asarray([x,y]), 100)
      actData.append(curY)
      apptData.append(myNN.evaluatePoint(curX))

   print actData
   print apprData

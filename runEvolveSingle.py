import numpy as np

from classes.neuralNet import NeuralNet
from classes.evolver import Evolver
import classes.dataSets as ds

evolve = Evolver()

pop = evolve.createPop(input_width=200, proc_width=40, proc_depth=3)
testXDat, testYDat = ds.genStereoDat(5000, 100)
inXDat, inYDat = ds.genStereoDat(1000, 100)
for t in xrange(1000):
   pop, curPrec = evolve.iterateGeneration(pop, inXDat, inYDat)
   curOutPrec = evolve.getFitness(pop[0], testXDat, testYDat)
   print str(t) + ": "
   print "in:\t", curPrec
   print "out:\t", curOutPrec

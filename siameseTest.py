import numpy as np
from classes.neuralNet import NeuralNet
from classes.evolver import Evolver

test = NeuralNet(input_width = 10, siam_width=10, siam_depth=3, proc_width=10, proc_depth=10)

x = np.arange(10)

test.evaluatePoint(x)

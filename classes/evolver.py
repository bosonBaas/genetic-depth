from neuralNet import NeuralNet
import random

class Evolver(object):
   
   def __init__(self, mutate_child_chance = 0.3, mutate_chance = 0.1, retain_rate = 0.3, random_select = 0.05, pop_size = 50, init_weight=2):
      self.mutate_child_chance = mutate_child_chance
      self.mutate_chance   = mutate_chance
      self.retain_rate     = retain_rate
      self.pop_size        = pop_size
      self.init_weight     = init_weight
      self.random_select   = min(random_select, 1-retain_rate)
      self.retain_num      = int(self.pop_size * self.retain_rate)
      self.random_num      = int(self.pop_size * self.random_select)

   def createPop(self, input_width, siam_width=0, siam_depth=0, proc_width=0 , proc_depth=0):
      pop = []

      for i in xrange(self.pop_size):
         temp = NeuralNet(input_width=input_width, siam_depth=siam_depth, siam_width=siam_width, proc_width=proc_width, proc_depth=proc_depth)
         temp.randomize(-self.init_weight, self.init_weight)
         pop.append(temp)
      return pop

   def getFitness(self, network, xData, yData):
      n, d = xData.shape
      
      totalError = 0;

      for i in xrange(n):
         yPred = network.evaluatePoint(xData[i])
         totalError += (yData[i] - yPred)**2
      return totalError / n
      

   def iterateGeneration(self, population, xData, yData):
      fitnesses = [(self.getFitness(x, xData, yData), x) for x in population]
      fitnesses.sort()

      oldPop = [x[1] for x in fitnesses[:self.retain_num]] 
      sample = random.sample(fitnesses[self.retain_num:], self.random_num)
      oldPop += [x[1] for x in sample]

      newPop = []

      while len(oldPop) + len(newPop) < self.pop_size:
         mother, father = random.sample(oldPop, 2)
         curChild = NeuralNet(input_width = mother.input_width, siam_width=mother.siam_width, siam_depth=mother.siam_depth, proc_width=mother.proc_width, proc_depth=mother.proc_depth)
         curChild.birth(mother, father)
         if random.random() < self.mutate_child_chance:
            curChild.mutate(self.mutate_chance, -self.init_weight, self.init_weight)
         newPop.append(curChild)

      return oldPop + newPop, fitnesses[0][0]


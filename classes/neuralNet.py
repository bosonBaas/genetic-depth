import numpy as np
import random

class NeuralNet(object):

   def __init__(self, input_width, siam_width=0, siam_depth=0, proc_width=0, proc_depth=0, debug=False):
      self.networkWeights = []
      self.siam_depth = siam_depth
      self.siam_width = siam_width
      self.proc_width = proc_width
      self.proc_depth = proc_depth
      self.input_width = input_width
      self.debug = debug
      
      if siam_width != 0 and input_width % 2 != 0:
         print "ERROR: INPUT CANNOT BE SPLIT EVENLY"
         return

      if siam_depth == 0:
         self.networkWeights.append(np.zeros((input_width + 1, proc_width)))
      else:
         self.networkWeights.append(np.zeros((input_width / 2 + 1, siam_width)))
         for i in xrange(1,siam_depth):
            self.networkWeights.append(np.zeros((siam_width + 1, siam_width)))
         if proc_depth == 0:
            self.networkWeights.append(np.zeros((siam_width * 2 + 1, 1)))
         else:
            self.networkWeights.append(np.zeros((siam_width * 2 + 1, proc_width)))
            

      if proc_depth != 0:
         for i in xrange(1, proc_depth):
            self.networkWeights.append(np.ones((proc_width + 1, proc_width)))
         self.networkWeights.append(np.ones((proc_width + 1, 1)))

   

   def __str__(self):
      count = 0;
      out = ""
      for layer in self.networkWeights:
         count += 1
         out += "layer: " + str(count) + '\n'
         for output in layer:
            for weight in output:
               out += str(weight)[0:4] + " "
            out += '\n'

      return out

   def randomize(self, minimum, maximum):
      for l in xrange(len(self.networkWeights)):
         layerShape = self.networkWeights[l].shape 
         self.networkWeights[l] = np.random.rand(*layerShape) * (maximum - minimum) + minimum
  
   def birth(self, mother, father):
      for l in xrange(len(self.networkWeights)):
         for n in xrange(self.networkWeights[l].shape[0]):
            if random.getrandbits(1):
               self.networkWeights[l][n] = mother.networkWeights[l][n]
            else:
               self.networkWeights[l][n] = father.networkWeights[l][n]
               
   def mutate(self, probability, minimum, maximum):
      for l in xrange(len(self.networkWeights)):
         for n in xrange(self.networkWeights[l].shape[0]):
            if random.random() < probability:
               shift = np.random.rand(*self.networkWeights[l][n].shape) * (maximum - minimum) + minimum
               self.networkWeights[l][n] += shift

   def evaluatePoint(self, point):
      s = point.reshape(-1,1)
      if self.siam_depth != 0:
         s1, s2 = np.split(s, 2)
         s2 = np.flipud(s2)
         for i in xrange(self.siam_depth):
            layer = self.networkWeights[i]
            if self.debug:
               print s1, s2
            s1 = np.vstack([s1,[1]])
            s1 = np.tanh(np.dot(np.transpose(layer), s1))
            s2 = np.vstack([s2, [1]])
            s2 = np.tanh(np.dot(np.transpose(layer), s2))
         s = np.vstack([s1,s2])

      for i in xrange(self.siam_depth, len(self.networkWeights)):
         if self.debug:
            print s
         layer = self.networkWeights[i]
         s = np.vstack([s,[1]])
         s = np.tanh(np.dot(np.transpose(layer), s))
      if self.debug:
         print s
      return s[0][0]

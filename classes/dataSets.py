import numpy as np
import math

def genCircleDat(num):
   xData = np.random.rand(num, 2) * 2 - 1
      
   yData = []
      
   for point in xData:
      if point[0] * point[0] + point[1] * point[1] < .5:
         yData.append(1)
      else:
         yData.append(-1)
   return xData, yData   

def genStereoDat(num, res):
   coordData = np.random.randn(num, 2)
   
   xData = np.zeros((num, res * 2))
   yData = []
   for i in xrange(len(coordData)):
      point = coordData[i]
      newX, newY = getStereo(point, res)
      xData[i] = newX
      yData.append(newY)
   return xData, yData

def genSizeDat(num, res):
   coordData = np.random.randn(num, 2)
   
   xData = np.zeros((num, res * 2))
   yData = []
   radius = 0.25
   for i in xrange(len(coordData)):
      point = coordData[i]
      dist = np.linalg.norm(point)
      dist1 = np.linalg.norm(point + [0.25, 0])
      dist2 = np.linalg.norm(point - [0.25, 0])
      while dist1 < radius or dist2 < radius:
         coordData[i] = np.random.randn(2)
         point = coordData[i]
         dist = np.linalg.norm(point)
         dist1 = np.linalg.norm(point + [0.25, 0])
         dist2 = np.linalg.norm(point - [0.25, 0])
      newX, newY = getSize(point, radius, res)
      xData[i] = newX
      yData.append(newY)
   return xData, yData

def getStereo(point, res):
   xData = np.zeros(res * 2)
   angle1 = math.atan2(abs(point[1]), point[0] + 0.25) / math.pi
   angle2 = math.atan2(abs(point[1]), point[0] - 0.25) / math.pi
   xData[int(res - angle1 * res)] = 1
   xData[min(int(res + res * angle2), res * 2 - 1)] = 1
   dist = np.linalg.norm(point)
   y = math.tanh(dist)
   return xData, y

def getSize(point, radius, res):
   xData = np.zeros(res * 2)
   dist = np.linalg.norm(point)
   dist1 = np.linalg.norm(point + [0.25, 0])
   dist2 = np.linalg.norm(point - [0.25, 0])
   size1 = np.arcsin(radius/dist1) / math.pi * res
   size2 = np.arcsin(radius/dist2) / math.pi * res
   for p in xrange(int(res/2 - size1), int(res/2 + size1)):
      xData[p] = 1

   for p in xrange(int(res + res/2 - size2), int(res + res/2 + size2)):
      xData[p] = 1
   
   return xData, math.tanh(dist)

import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import pickle

def plotArr(folder, arg, n):
   toPlot = []
   for file in os.listdir(folder):
      if file.startswith(arg):
         with open(folder + file, 'rb') as f:
            toPlot.append(pickle.load(f))
   toPlot = np.asarray(sorted(toPlot, key=lambda x: x[0][-1]))
   x = [i + 1 for i in xrange(len(toPlot[0][0]) - 1)]
   for i in xrange(len(toPlot)):
      mean = np.mean(toPlot[i],0)[:-1]
      stdDev = np.std(toPlot[i],0)[:-1]
      print toPlot[i][:]
      plt.plot(x, mean, label=str(toPlot[i][0][-1]))
      plt.fill_between(x, mean - stdDev, mean + stdDev, alpha=0.5)
   plt.legend()
   plt.show()

folder = sys.argv[1]
arg = sys.argv[2]
if len(sys.argv) == 4:
   numEl = int(sys.argv[3])
else:
   numEl = 1

plotArr(folder, arg, numEl)

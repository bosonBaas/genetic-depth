import random
import sys
import numpy as np
import matplotlib.pyplot as plt
from classes.neuralNet import NeuralNet
from classes.evolver import Evolver

testEvolve = Evolver(mutate_child_chance=1.0, mutate_chance=0.1)
testPop = testEvolve.createPop(2, 10, 2)

xData = np.random.rand(100, 2) * 2 - 1

a = random.random() * 10
b = random.random()

yData = []

for point in xData:
   if point[1] > a * point[0] + b:
      yData.append(1)
   else:
      yData.append(-1)

for t in xrange(1000):
   testPop, curPrec = testEvolve.iterateGeneration(testPop, xData, yData)
   print(curPrec)


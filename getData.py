import sys
import numpy as np
import pickle
from classes.neuralNet import NeuralNet
from classes.evolver import Evolver
import classes.dataSets as ds

def varyEvolve(arg, vals, numIter, numGen):
   outData = [] 
   testXDat, testYDat = ds.genCircleDat(500)
   
   for value in vals:
      evolve = Evolver(**{arg:value})
      for j in xrange(numIter):
         outData.append([])
         pop = evolve.createPop(input_width=2, proc_width=10, proc_depth=2)
         inXDat, inYDat = ds.genCircleDat(250)
         
         for t in xrange(numGen):
            pop, curPrec = evolve.iterateGeneration(pop, inXDat, inYDat)
            outData[-1].append(evolve.getFitness(pop[0], testXDat, testYDat))
            print arg + ":" + str(value) + ":" + str(j) + ":" + str(t)
            print '\t' + str(outData[-1][t])
            print '\t' + str(curPrec)
      
         outData[-1].append(value)
      with open("data/" + arg + "_" + str(value)
                  + "_g" + str(numGen) 
                  + "_i" + str(numIter) 
                  + ".dat", 'wb') as f:
         pickle.dump(outData, f)
         outData = []
   

  

vals = range(int(sys.argv[1]), int(sys.argv[2]) + 1)
numIter = int(sys.argv[3])
numGen = int(sys.argv[4])

#varyEvolve("mutate_child_chance", [x * 0.1 for x in vals], numIter, numGen)
#varyEvolve("mutate_chance", [x * 0.1 for x in vals], numIter, numGen)
#varyEvolve("retain_rate", [x * 0.1 for x in vals], numIter, numGen)
varyEvolve("random_select", [x * 0.1 for x in vals], numIter, numGen)
#varyEvolve("init_weight", [x * 0.5 for x in vals], numIter, numGen)


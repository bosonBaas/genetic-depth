import sys
import numpy as np
import pickle
from classes.neuralNet import NeuralNet
from classes.evolver import Evolver
import classes.dataSets as ds

def varyEvolve(depth, widthSiam, widthProc, numIter, numGen):
   outData = [] 
   testXDat, testYDat = ds.genStereoDat(500, 100)
   
   for i in xrange(depth + 1):
      evolve = Evolver()
      for j in xrange(numIter):
         outData.append([])
         pop = evolve.createPop(input_width=200, siam_width= widthSiam, siam_depth=i, proc_width=widthProc, proc_depth=depth - i)
         inXDat, inYDat = ds.genStereoDat(300, 100)
         
         for t in xrange(numGen):
            pop, curPrec = evolve.iterateGeneration(pop, inXDat, inYDat)
            outData[-1].append(evolve.getFitness(pop[0], testXDat, testYDat))
            print "depth:" + str(depth) + ":" + str(i) + ":" + str(j) + ":" + str(t)
            print '\t' + str(outData[-1][t])
            print '\t' + str(curPrec)
      
         outData[-1].append(i)
      with open("data/" + "depth:" +str(depth) +"_" + str(i)
                  + "_g" + str(numGen) 
                  + "_i" + str(numIter) 
                  + ".dat", 'wb') as f:
         pickle.dump(outData, f)
         outData = []
   

  

depth = int(sys.argv[1])
numIter = int(sys.argv[2])
numGen = int(sys.argv[3])

varyEvolve(depth, 10, 10, numIter, numGen)
